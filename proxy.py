#!/usr/bin/env python3

import sys
import threading
import socket
import select
import re
import time
BUFFER_SIZE = 12582912  ##12 MiB
bad_words   = ["spongebob", "britney spears", "paris hilton", "norrkoping", "norrk%C3%B6ping"]
error1      = bytearray(b"HTTP/1.1 302 Found\r\nLocation: http://www.ida.liu.se/~TDTS04/labs/2011/ass2/error1.html\r\n\r\n\r\n")
error2      = bytearray(b"HTTP/1.1 302 Found\r\nLocation: http://www.ida.liu.se/~TDTS04/labs/2011/ass2/error2.html\r\n\r\n\r\n")


def found_bad_words(string):
    for word in bad_words:
        if word in string.lower():
            return True
    return False


def forward(dataBuffer, rxSocket):
    while len(dataBuffer) > 0:
        sent = rxSocket.send(dataBuffer)
        if sent == b'':
            print("Error in forward(): Broken connection")
            rxSocket.close()
            break
        del dataBuffer[0:sent]


def get_host(data):
    host_header = re.search('host: ([\w.\-]+)(?::(\d*))?', data.decode("ISO-8859-1"), re.IGNORECASE)
    regex_host, regex_port = None, None
    if host_header:
        regex_host = host_header.group(1)
        regex_port = host_header.group(2)
    if regex_port is None:
        regex_port = 80
    return regex_host, regex_port


class Connection:
    def __init__(self, client, pid):
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client = client
        self.sockets = [client]
        self.host, self.port = None, None
        self.clientBuff, self.serverBuff = bytearray(b''), bytearray(b'')
        self.content_length = 0
        self.connection_type = ""
        self.pid = pid

    def setupClientRequest(self):
        regex_host, regex_port = get_host(self.clientBuff)
        if regex_host is not None:
           # print(self.pid, "Host header found:", regex_host, regex_port)
            if regex_host != self.host or regex_port != self.port:
                self.host, self.port = regex_host, regex_port
                print(self.pid, "\tConnecting to", self.host, ":" + str(self.port))
                if self.server in self.sockets:
                    self.sockets.remove(self.server)
                self.server.close()
                self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.server.connect((self.host, int(regex_port)))
                self.server.settimeout(60)
                if self.server not in self.sockets:
                    self.sockets.append(self.server)

        url = re.search('GET (.*) HTTP/1.[0|1]', self.clientBuff.decode("ISO-8859-1"), re.IGNORECASE)
        if url:
            self.serverBuff = bytearray(b'')
            url = url.group(1)
           # print(self.pid, " ", url[:25])
            if found_bad_words(url):
                print(self.pid, "Bad URL")
                self.clientBuff = bytearray(b'')
                self.client.send(error1)

    def extract_header_info(self):
        rawHeader = self.serverBuff.split(b'\r\n\r\n'or b'\n\n')[0]
        header = rawHeader.decode("ISO-8859-1")

        split_length = len(b'\r\n\r\n') if b'\r\n\r\n' in self.serverBuff else b'\n\n'
        match1 = re.search('content-length: (\d+)', header, re.IGNORECASE)
        self.content_length = int(match1.group(1)) + len(header) + split_length if match1 else 0

        match2 = re.search('connection: (.*)', header, re.IGNORECASE)
        self.connection_type = match2.group(1) if match2 else "close"

        match3 = re.search('content-type: (\w*/\w*)', header, re.IGNORECASE)
        return match3.group(1) if match3 else "unknown"

    def close(self):
        print("X", self.pid, "\tClosed connection ")
        self.server.close()
        self.client.close()


def new_client(client, address, pid):
    print(pid, "\tClient connected", address)
    c = Connection(client, pid)
    overflow = False
    header_found = False
    send_request = False
    while True:
        inputready, outputready, exceptready = select.select(c.sockets, c.sockets, c.sockets, 60)

        for s in inputready:
            data = s.recv(4096)
            #if data == b'': 
            #   c.close()  Should be catched by exceptready
            #  return
            if s is c.client:
                c.clientBuff += data
                if b'\r\n\r\n' in c.clientBuff or b'\n\n' in c.clientBuff:
                    c.setupClientRequest()
                    header_found = False
                    send_request = True

            elif s is c.server:
                c.serverBuff += data
                if not header_found and (b'\r\n\r\n' or b'\n\n'in c.serverBuff):
                    header_found = True  # Found header!
                    overflow = False
                    data_type = c.extract_header_info()
                    if data_type and "text/" not in data_type:
                        overflow = True  # Not a text file

        for s in outputready:
            if s is c.client and header_found:
                if c.content_length is 0 or overflow:
                    forward(c.serverBuff, c.client)
                elif (len(c.serverBuff) >= c.content_length or len(c.serverBuff) >= BUFFER_SIZE):
                    if found_bad_words(c.serverBuff.decode("ISO-8859-1")):
                        c.serverBuff = bytearray(b'')
                        c.client.send(error2)
                    overflow = True
            elif s is c.server and send_request:
                forward(c.clientBuff, c.server)
                send_request = False
            time.sleep(0.02)  #reduce cpu usage

        for s in exceptready:
            c.close()
            print >>sys.stderr
            return

##############
## Main thread
if len(sys.argv) != 2:
    print("Invalid number of arguments")
    exit(1)

port = int(sys.argv[1])
serverSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
serverSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
serverSocket.bind(('0.0.0.0', port))
serverSocket.listen(5)
pid = 0
print("[pid]\t[Event]")
try:
    while True:
        client, address = serverSocket.accept()
        client.settimeout(30)
        pid += 1
        threading.Thread(target=new_client, args=(client, address, pid)).start()
except KeyboardInterrupt:
    print("[*] Server shutdown by Ctrl-c.")
